var express = require('express'), app = express();
var port = process.env.PORT || 3000;
console.log( 'Puerto: ' + port );
var ambiente = process.env.AMBIENTE || "desarrollo";
console.log( 'Ambiente: ' + ambiente );
var requestjson = require('request-json');
var urlS3 = "https://s3-us-west-2.amazonaws.com/ptup-ajj-jamh/usuarios.json";
var clienteS3 = requestjson.createClient( urlS3 );

var bodyParser = require('body-parser');
app.use( bodyParser.json() );
var cors = require('cors');
app.options( '*', cors() );

let date = require( 'date-and-time' );

app.listen( port );
console.log( 'API GT started on..' );

/*app.get( "/gt/usuario", function( req, res ) {
  console.log( "GET /gt/usuario" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  clienteS3.get( "", "", function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );*/

app.post( "/gt/usuario", function( req, res ) {
  console.log( "POST /gt/usuario" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  var idusuario = req.body.idusuario;
  var password = req.body.password;
  var respuesta = { "error" : "Usuario y/o password incorrecto." };
  if( idusuario && password ){
    clienteS3.get( "", "", function( error, response, body ){
      console.log('error:', error);
      console.log('statusCode:', response && response.statusCode);
      console.log('body:', body);
      if( !error ){
        console.log( "Usuarios:" , JSON.stringify( body.usuarios ) );
        for( var us in body.usuarios ){
          if( ( body.usuarios[us].idusuario == idusuario ) && ( body.usuarios[us].password == password ) ){
            var f1 = new Date();
            var f2 = date.addMinutes( f1, 10 );
            var tdata = {
              fechaInicio : f1,
              minutosVigencia : 10,
              fechaFin : f2
            }
            body.usuarios[us].tokendata = tdata;
            body.usuarios[us].token = Buffer.from( JSON.stringify( body.usuarios[us] ) ).toString( "base64" );
            respuesta = body.usuarios[us];
            break;
          }
        }
      }
      res.send( respuesta );
    } );
  } else {
    res.send( respuesta );
  }
} );

app.post( "/gt/usuario/validar", function( req, res ) {
  console.log( "POST /gt/usuario/validar" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  var token = req.body.token;
  var respuesta = { valido : false, error : "Token invalido." };
  if( token ){
    //console.log( 'Token String:', Buffer.from( token, "base64" ).toString() );
    var usr = JSON.parse( Buffer.from( token, "base64" ).toString() );
    console.log( "Usuario:" , JSON.stringify( usr ) );//2018-10-17T09:42:47.398Z
    var f1 = date.subtract( date.parse( usr.tokendata.fechaFin, "YYYY-MM-DDTHH:mm:ss.SSSZ", true ), new Date() ).toMilliseconds();
    if( f1 > 0 ) {
      clienteS3.get( "", "", function( error, response, body ){
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        console.log('body:', body);
        if( !error ){
          console.log( "Usuarios:" , JSON.stringify( body.usuarios ) );
          for( var us in body.usuarios ){
            if( ( body.usuarios[us].idusuario == usr.idusuario ) && ( body.usuarios[us].password == usr.password ) ){
              respuesta = { valido : true };
              break;
            }
          }
        }
        res.send( respuesta );
      } );
    } else {
      res.send( respuesta );
    }
  } else {
    res.send( respuesta );
  }
} );
